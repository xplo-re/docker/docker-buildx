ARG DOCKER_VERSION=stable

FROM docker:${DOCKER_VERSION}
COPY --from=docker/buildx-bin /buildx /usr/libexec/docker/cli-plugins/docker-buildx

# Add coreutils for version sort, jq to extract data from Docker JSON outputs
# and skopeo for advanced image operations that Docker itself lacks.
RUN apk add --no-cache ca-certificates coreutils git jq skopeo