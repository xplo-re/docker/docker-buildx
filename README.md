# Docker Images with Buildx and Tools

Docker images designed for running Docker-in-Docker automation tasks that target
multiple platforms.

## Image Contents
Based on the latest official [`docker`][docker] and [`docker/buildx-bin`][buildx] images.

OS is Alpine Linux.

Takes the official `docker` image, installs the latest version of the `buildx`
plugin from the official `docker/buildx-bin` image, followed by some additional
frequently used Alpine packages:
- `ca-certificates` \
  Common CA certificates PEM files from Mozilla.
- `coreutils` \
  Basic file, shell and text manipulation utilities.
- `git` \
  Distributed version control system.
- `jq` \
  Lightweight command-line based JSON processor.
- `skopeo` \
  Command-line based tool that performs various operations on Docker container
  images and (remote) Docker image repositories that Docker itself does not
  provide.

## Usage
Refer to the following registry when requesting an image:
```
registry.gitlab.com/xplo-re/docker/docker-buildx
```

## Tags
Currently images for Docker version `19` and `20` are provided.

**[List of all available tags][taglist]**

Tags are created as follows:
- `19`, `20`, `latest` \
  Docker version (with alias `latest` for the latest version of Docker).
  Contains the latest version of `buildx`.
- `19-#.#.#`, `20-#.#.#` \
  Docker version, followed by full `buildx` version.
- `#.#.#` \
  Full Docker version. Contains the latest version of `buildx`.
- `#.#.#-#.#.#` \
  Full Docker version and full `buildx` version.

## Release Schedule
Images are built on a daily schedule.

For new versions of Docker and `buildx`, a new tag is automatically created.

[docker]: https://hub.docker.com/_/docker
[buildx]: https://hub.docker.com/r/docker/buildx-bin
[taglist]: https://gitlab.com/xplo-re/docker/docker-buildx/container_registry